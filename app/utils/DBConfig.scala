package utils

import com.google.inject.Singleton
import slick.driver.H2Driver
import slick.driver.H2Driver.api._
import slick.jdbc.meta.MTable
import slick.tables.Cars._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by natali on 17.11.16.
  */
trait DBConfig {

  def objDBConf: DBConf

  trait DBConf {

    def db: H2Driver.backend.DatabaseDef

    def setup: Future[Seq[Unit]]
  }

}

trait DBConfigImpl extends DBConfig {

  def objDBConf = new DBConfImpl

  @Singleton
  class DBConfImpl extends DBConf {

    lazy val db: H2Driver.backend.DatabaseDef = Database.forConfig("h2mem1")

    override def setup = createTablesIfNotExists(TCars)

    def createTablesIfNotExists(tables: TableQuery[_ <: Table[_]]*): Future[Seq[Unit]] = {
      Future.sequence(
        tables map { table =>
          db.run(MTable.getTables(table.baseTableRow.tableName)).flatMap { result =>
            if (result.isEmpty) {
              db.run(table.schema.create)
            } else {
              Future.successful(())
            }
          }
        }
      )
    }
  }

}