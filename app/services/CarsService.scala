package services

import java.util.Calendar

import dao.CarsDAO
import models.DBCar
import models.enumerations.{Fuel, Property}
import play.api.libs.json.{Format, Json}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

/**
  * Created by natali on 17.11.16.
  */
case class Car(id: Option[Int],
               title: String,
               fuel: Fuel.Value,
               price: Int,
               isNew: Boolean,
               mileage: Option[Int],
               firstReg: Option[Long])

object Car {

  def toDBCar(car: Car): DBCar = DBCar(
    car.id,
    car.title,
    car.fuel.toString,
    car.price,
    car.isNew,
    car.mileage,
    car.firstReg)

  def toServiceCar(dbCar: DBCar): Car = Car(
      dbCar.id,
      dbCar.title,
      Fuel.withName(dbCar.fuel),
      dbCar.price,
      dbCar.isNew,
      dbCar.mileage,
      dbCar.firstReg)

  implicit val json: Format[Car] = Json.format[Car]
}

trait CarsService {

  def objCarService: CarService

  trait CarService {

    def create(car: Car): Future[Int]

    def update(car: Car): Future[Int]

    def delete(id: Int): Future[Int]

    def getById(id: Int): Future[Option[Car]]

    def getByFilter(property: Property.Value): Future[List[Car]]

    def mock: Future[Unit]
  }

}

trait CarsServiceImpl extends CarsService {
  this: CarsDAO =>

  def objCarService = new CarServiceImpl

  class CarServiceImpl extends CarService {

    def create(car: Car): Future[Int] = for {
      created <- objCarDAO.create(Car.toDBCar(car))
    } yield created

    def update(car: Car): Future[Int] = for {
      updated <- objCarDAO.update(Car.toDBCar(car))
    } yield updated

    def delete(id: Int): Future[Int] = for {
      deleted <- objCarDAO.delete(id)
    } yield deleted

    def getById(id: Int): Future[Option[Car]] = for {
      res <- objCarDAO.getById(id)
    } yield res.map(car => Car.toServiceCar(car))

    def getByFilter(property: Property.Value): Future[List[Car]] = for {
      cars <- objCarDAO.sortedByProperty(property)
    } yield cars.map(car => Car.toServiceCar(car))

    def mock: Future[Unit] = {

      create(Car(None, "Volkswagen", Fuel.GASOLINE, Random.nextInt(), isNew = true, None, None))

      create(Car(None, "Audi", Fuel.DIESEL, Random.nextInt(), isNew = false, Some(Random.nextInt()), Some(Calendar.getInstance().getTimeInMillis)))

      Future(Unit)
    }
  }

}
