package slick.tables

import models.DBCar
import slick.driver
import slick.jdbc.{GetResult => GR}

/**
  * Created by natali on 17.11.16.
  */

object Cars extends {
  val profile = driver.H2Driver
} with Cars

trait Cars {
  val profile: driver.JdbcProfile

  import profile.api._

  lazy val schema: profile.SchemaDescription = Array(TCars.schema).reduceLeft(_ ++ _)

  implicit def GetResultAgentsRow(implicit e0: GR[Option[Int]], e1: GR[Long]): GR[DBCar] = GR {
    prs => import prs._
      DBCar.tupled((<<[Option[Int]], <<[String], <<[String], <<[Int], <<[Boolean], <<[Option[Int]], <<[Option[Long]]))
  }

  class Cars(_tableTag: Tag) extends Table[DBCar](_tableTag, "car") {

    def * = (id, title, fuel, price, isNew, mileage, date) <>(DBCar.tupled, DBCar.unapply)

    def ? = (id, Rep.Some(title), Rep.Some(fuel), Rep.Some(price), Rep.Some(isNew), mileage, date).shaped.<>({ r => import r._; _1.map(_ => DBCar.tupled((_1, _2.get, _3.get, _4.get, _5.get, _6, _7))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    val id: Rep[Option[Int]] = column[Option[Int]]("id", O.AutoInc, O.PrimaryKey)
    val title: Rep[String] = column[String]("title", O.Length(45, varying=true))
    val fuel: Rep[String] = column[String]("fuel", O.Length(45, varying=true))
    val price: Rep[Int] = column[Int]("price", O.Length(45, varying=true))
    val isNew: Rep[Boolean] = column[Boolean]("isNew", O.Length(45, varying=true))
    val mileage: Rep[Option[Int]] = column[Option[Int]]("mileage")
    val date: Rep[Option[Long]] = column[Option[Long]]("date")
  }

  lazy val TCars = new TableQuery(tag => new Cars(tag))

}
