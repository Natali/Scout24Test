package controllers

import dao.CarsDAOImpl
import services.Car
import models.enumerations.Property
import play.api.libs.json._
import play.api.mvc.{Action, Controller}
import services.CarsServiceImpl
import utils.DBConfigImpl

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by natali on 17.11.16.
  */
class CarsController extends Controller with CarsServiceImpl with CarsDAOImpl with DBConfigImpl {

  def mock = Action.async {
    objCarService.mock
    Future(Ok(Json.toJson("mocked")))
  }

  def createDb = Action.async {
    objDBConf.setup
    Future(Ok(Json.toJson("Database is created")))
  }

  def create = Action.async(parse.json) {
    implicit request =>
      request.body.validate(Car.json) match {
        case JsError(_) => Future(BadRequest("Can`t create a car"))
        case JsSuccess(car, _) =>
          for {
            res <- objCarService.create(car)
          } yield {
            Ok(Json.toJson(Json.obj("status" -> res, "message" -> s"""Car ${car.id} is created""")))
          }
      }
  }

  def update = Action.async(parse.json) {
    implicit request =>
      request.body.validate(Car.json) match {
        case JsError(_) => Future(BadRequest("Can`t update a car"))
        case JsSuccess(car, _) =>
          for {
            res <- objCarService.update(car)
          } yield {
            Ok(Json.toJson(Json.obj("status" -> res, "message" -> s"""Car ${car.id} is updated""")))
          }
      }
  }

  def delete(id: Int) = Action.async {
    implicit request =>
      for {
        res <- objCarService.delete(id)
      } yield {
        Ok(Json.toJson(Json.obj("status" -> res, "message" -> s"""Car $id is deleted""")))
      }
  }

  def getById(id: Int) = Action.async {
    implicit request =>
      val result = for {
        res <- objCarService.getById(id)
      } yield {
        res match {
          case Some(car) => Ok(Json.toJson(car)(Car.json))
          case None => Ok(Json.toJson(Json.obj("errorMessage" -> s"""Can`t get car by id $id""" )))
        }
      }
      result
  }

  def getByFilter(property: Int) = Action.async {
    implicit request =>
      val result = for {
        res <- objCarService.getByFilter(Property(property))
      } yield {
        Ok(Json.toJson(res.map(Json.toJson(_)(Car.json))))
      }
      result
  }

}
