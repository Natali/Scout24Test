package models

/**
  * Created by natali on 17.11.16.
  */
case class DBCar(id: Option[Int],
                 title: String,
                 fuel: String,
                 price: Int,
                 isNew: Boolean,
                 mileage: Option[Int],
                 firstReg: Option[Long])
