package models.enumerations

import play.api.libs.json.{Format, JsResult, JsValue, Json}

/**
  * Created by natali on 17.11.16.
  */
object Fuel extends Enumeration {
  type Fuel = Value

  val GASOLINE = Value(0)
  val DIESEL = Value(1)

  implicit val format = new Format[Fuel.Value] {
    override def writes(o: Fuel.Value): JsValue = Json.toJson(o.toString)
    override def reads(json: JsValue): JsResult[Fuel.Value] = json.validate[String].map(Fuel.withName)
  }
}

object Property extends Enumeration {

  type Property = Value
  val ID = Value(0)
  val TITLE = Value(1)
  val FUEL = Value(2)
  val PRICE = Value(3)
  val IS_NEW = Value(4)
  val MILEAGE = Value(5)
  val FIRST_REG = Value(6)

}