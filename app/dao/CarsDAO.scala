package dao

import models.DBCar
import models.enumerations.Property
import slick.tables.Cars._
import utils.DBConfig
import play.api.Logger
import slick.driver.H2Driver.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


/**
  * Created by natali on 17.11.16.
  */
trait CarsDAO {

  def objCarDAO: CarDAO

  trait CarDAO {

    def create(car: DBCar): Future[Int]

    def update(car: DBCar): Future[Int]

    def delete(id: Int): Future[Int]

    def getById(id: Int): Future[Option[DBCar]]

    def sortedByProperty(property: Property.Value): Future[List[DBCar]]
  }

}

trait CarsDAOImpl extends CarsDAO {

  this: DBConfig =>

  def objCarDAO = new CarDAOImpl

  class CarDAOImpl extends CarDAO {

    def create(car: DBCar): Future[Int] = {
      Logger.debug("Create new car " + car)
      objDBConf.db.run(TCars returning TCars.map(_.id.get) += car).mapTo[Int]
    }

    def update(car: DBCar): Future[Int] = {
      Logger.debug("Update car " + car)
      objDBConf.db.run(TCars.filter(_.id === car.id).update(car))
    }

    def delete(id: Int): Future[Int] = {
      Logger.debug("Delete car " + id)
      objDBConf.db.run(TCars.filter(_.id === id).delete)
    }

    def getById(id: Int): Future[Option[DBCar]] = {
      objDBConf.db.run(TCars.filter(_.id === id).result.headOption)
    }

    def sortedByProperty(property: Property.Value): Future[List[DBCar]] = {
      property match {
        case Property.TITLE => objDBConf.db.run(TCars.sortBy(_.title).result).map(_.toList)
        case Property.FUEL => objDBConf.db.run(TCars.sortBy(_.fuel).result).map(_.toList)
        case Property.PRICE => objDBConf.db.run(TCars.sortBy(_.price).result).map(_.toList)
        case Property.IS_NEW => objDBConf.db.run(TCars.sortBy(_.isNew).result).map(_.toList)
        case Property.MILEAGE => objDBConf.db.run(TCars.sortBy(_.mileage).result).map(_.toList)
        case Property.FIRST_REG => objDBConf.db.run(TCars.sortBy(_.date).result).map(_.toList)
        case _ => objDBConf.db.run(TCars.sortBy(_.id).result).map(_.toList)
      }

    }
  }

}
